package com.test.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "title")
    @NotNull
    private String title;

    @Column(name = "authorFirstName")
    @NotNull
    private String authorFirstName;

    @Column(name = "authorLastName")
    @NotNull
    private String authorLastName;

    @Column(name = "publisher")
    @NotNull
    private String publisher;

    @Column(name = "yyyyPublished")
    @NotNull
    private int yyyyPublished;

    @Column(name = "format")
    @NotNull
    private String format;

}
