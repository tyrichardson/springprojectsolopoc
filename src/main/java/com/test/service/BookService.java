package com.test.service;

import com.test.model.Book;
import java.util.Optional;

public interface BookService {

    Iterable<Book> getBooks();
    Optional<Book> getBook(int id) throws Exception;
    Book addBook(Book b) throws Exception;
    Book updateBook(int id, Book b) throws Exception;
    void deleteBook(int id) throws Exception;
}
