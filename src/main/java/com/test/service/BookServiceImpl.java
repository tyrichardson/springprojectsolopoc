package com.test.service;

import java.util.Optional;

import com.test.model.Book;
import com.test.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository repo;

    @Override
    public Iterable<Book> getBooks() {
        return repo.findAll();
    }

    @Override
    public Optional<Book> getBook(int id) throws Exception {
        if (repo.existsById(id)) {
            return repo.findById(id);
        } else {
            throw new Exception("Not found.");
        }
    }

    @Override
    public Book addBook(Book b) throws Exception {
        if (b.getId() == null) {
            return repo.save(b);
        } else {
            throw new Exception("ID was provided. Please omit ID field.");
        }
    }

    @Override
    public Book updateBook(int id, Book b) throws Exception{
        if (repo.existsById(id)) {
            b.setId(id);
            return repo.save(b);
        } else {
            throw new Exception("Book does not exist.");
        }
    }

    @Override
    public void deleteBook(int id) throws Exception {
        if (repo.existsById(id)) {
            repo.deleteById(id);
        } else {
            throw new Exception("Book does not exist.");
        }
    }

}
