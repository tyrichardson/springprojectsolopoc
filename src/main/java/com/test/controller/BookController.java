package com.test.controller;

import java.util.Optional;

import com.test.model.Book;
import com.test.service.BookService;
import com.test.repository.BookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

// annotation for rest com.test.controller
@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping(path = "/book")

public class BookController {

    @Autowired
    BookService service;
    BookRepository repo;




    @GetMapping("")
    public ResponseEntity<Iterable<Book>> getBooks() {
        return new ResponseEntity<>(service.getBooks(), HttpStatus.OK);
    }

    @GetMapping("{id}")
    public ResponseEntity<Optional<Book>> getBook(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getBook(id), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("")
    public ResponseEntity<Book> addBook(@RequestBody Book b) {
        try {
            return new ResponseEntity<>(service.addBook(b), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("{id}")
    public ResponseEntity<Book> updateBook(@PathVariable int id, @RequestBody Book b) {
        try {
            return new ResponseEntity<>(service.updateBook(id, b), HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Book> deleteBook(@PathVariable int id) {
        try {
            service.deleteBook(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
